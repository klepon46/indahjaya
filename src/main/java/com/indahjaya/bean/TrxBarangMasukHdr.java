/**
 * 
 */
package com.indahjaya.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

/**
 * @author garya
 *
 */

@Entity
@Table(name="TRX_BARANG_MASUK_HDR")
public class TrxBarangMasukHdr implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_TRANSAKSI")
	private String kodeTransaksi;

	@Column(name="JUMLAH_BARANG")
	private Integer jumlahBarang;

	@Column(name="TOTAL_HARGA")
	private BigDecimal totalHarga;

	@CreatedDate
	@Column(name="CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@CreatedBy
	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@Column(name="KETERANGAN")
	private String keterangan;
	
	@Column(name="NAMA")
	private String nama;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="trxBarangMasukHdr")
	private Set<TrxBarangMasukDtl> trxBarangMasukDtls;

	public TrxBarangMasukHdr() {
		// TODO Auto-generated constructor stub
	}

	public String getKodeTransaksi() {
		return kodeTransaksi;
	}

	public void setKodeTransaksi(String kodeTransaksi) {
		this.kodeTransaksi = kodeTransaksi;
	}

	public Integer getJumlahBarang() {
		return jumlahBarang;
	}

	public void setJumlahBarang(Integer jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}

	public BigDecimal getTotalHarga() {
		return totalHarga;
	}

	public void setTotalHarga(BigDecimal totalHarga) {
		this.totalHarga = totalHarga;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Set<TrxBarangMasukDtl> getTrxBarangMasukDtls() {
		return trxBarangMasukDtls;
	}

	public void setTrxBarangMasukDtls(Set<TrxBarangMasukDtl> trxBarangMasukDtls) {
		this.trxBarangMasukDtls = trxBarangMasukDtls;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeTransaksi == null) ? 0 : kodeTransaksi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxBarangMasukHdr other = (TrxBarangMasukHdr) obj;
		if (kodeTransaksi == null) {
			if (other.kodeTransaksi != null)
				return false;
		} else if (!kodeTransaksi.equals(other.kodeTransaksi))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxBarangMasukHdr [kodeTransaksi=" + kodeTransaksi + "]";
	}

}
