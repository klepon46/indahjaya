/**
 * 
 */
package com.indahjaya.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

/**
 * @author garya
 *
 */

@Entity
@Table(name="TRX_BARANG_MASUK_DTL")
public class TrxBarangMasukDtl implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrxBarangMasukDtlPk pk;
	
	@Column(name="NAMA_BARANG")
	private String namaBarang;
	
	@Column(name="QTY")
	private Integer qty;
	
	@Column(name="HARGA")
	private BigDecimal Harga;
	
	@CreatedDate
	@Column(name="CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@CreatedBy
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="KODE_TRANSAKSI", referencedColumnName="KODE_TRANSAKSI", updatable=false, insertable=false)
	private TrxBarangMasukHdr trxBarangMasukHdr;
	
	public TrxBarangMasukDtl() {
		// TODO Auto-generated constructor stub
	}

	public TrxBarangMasukDtlPk getPk() {
		if(pk == null)
			pk = new TrxBarangMasukDtlPk();
		return pk;
	}

	public void setPk(TrxBarangMasukDtlPk pk) {
		this.pk = pk;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public BigDecimal getHarga() {
		return Harga;
	}

	public void setHarga(BigDecimal harga) {
		Harga = harga;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public TrxBarangMasukHdr getTrxBarangMasukHdr() {
		return trxBarangMasukHdr;
	}

	public void setTrxBarangMasukHdr(TrxBarangMasukHdr trxBarangMasukHdr) {
		this.trxBarangMasukHdr = trxBarangMasukHdr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxBarangMasukDtl other = (TrxBarangMasukDtl) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxBarangMasukDtl [pk=" + pk + "]";
	}

}
