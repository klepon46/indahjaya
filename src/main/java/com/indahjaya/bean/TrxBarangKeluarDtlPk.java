package com.indahjaya.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxBarangKeluarDtlPk implements Serializable {

	private static final long serialVersionUID = 5681047381764545430L;

	@Column(name="KODE_TRANSAKSI")
	private String kodeTransaksi;
	
	@Column(name="KODE_BARANG")
	private String kodeBarang;
	
	public TrxBarangKeluarDtlPk() {
		// TODO Auto-generated constructor stub
	}

	public String getKodeTransaksi() {
		return kodeTransaksi;
	}

	public void setKodeTransaksi(String kodeTransaksi) {
		this.kodeTransaksi = kodeTransaksi;
	}

	public String getKodeBarang() {
		return kodeBarang;
	}

	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeBarang == null) ? 0 : kodeBarang.hashCode());
		result = prime * result
				+ ((kodeTransaksi == null) ? 0 : kodeTransaksi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrxBarangKeluarDtlPk other = (TrxBarangKeluarDtlPk) obj;
		if (kodeBarang == null) {
			if (other.kodeBarang != null)
				return false;
		} else if (!kodeBarang.equals(other.kodeBarang))
			return false;
		if (kodeTransaksi == null) {
			if (other.kodeTransaksi != null)
				return false;
		} else if (!kodeTransaksi.equals(other.kodeTransaksi))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrxBarangKeluarDtlPk [kodeTransaksi=" + kodeTransaksi
				+ ", kodeBarang=" + kodeBarang + "]";
	}
	
	
}
