/**
 * 
 */
package com.indahjaya.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author garya
 *
 */
@Entity
@Table(name="MST_RUNNUM")
public class MstRunnum {
	
	@Id
	@Column(name="PREFIX")
	private String prefix;
	
	@Column(name="SEQUENCE")
	private Integer sequence;
	
	@Column(name="MIDFIX")
	private String midfix;
	
	public MstRunnum() {
		// TODO Auto-generated constructor stub
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getMidfix() {
		return midfix;
	}

	public void setMidfix(String midfix) {
		this.midfix = midfix;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MstRunnum other = (MstRunnum) obj;
		if (prefix == null) {
			if (other.prefix != null)
				return false;
		} else if (!prefix.equals(other.prefix))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MstRunnum [prefix=" + prefix + "]";
	}
	
	
}
