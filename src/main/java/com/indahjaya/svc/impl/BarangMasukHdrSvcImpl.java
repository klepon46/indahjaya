/**
 * 
 */
package com.indahjaya.svc.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.TrxBarangMasukHdr;
import com.indahjaya.repo.TrxBarangMasukHdrRepo;
import com.indahjaya.svc.BarangMasukHdrSvc;

/**
 * @author garya
 *
 */

@Service(value="barangMasukHdrSvc")
@Transactional
public class BarangMasukHdrSvcImpl implements BarangMasukHdrSvc {

	@Autowired
	private TrxBarangMasukHdrRepo trxBarangMasukHdrRepo;

	public void save(TrxBarangMasukHdr object) {
		trxBarangMasukHdrRepo.save(object);
	}

	public void saves(Collection<TrxBarangMasukHdr> objects) {
		trxBarangMasukHdrRepo.save(objects);
	}

	public List<TrxBarangMasukHdr> findAll() {
		return trxBarangMasukHdrRepo.findAll();
	}

}
