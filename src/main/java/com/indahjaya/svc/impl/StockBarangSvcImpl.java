package com.indahjaya.svc.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.MstStock;
import com.indahjaya.repo.MstStockRepo;
import com.indahjaya.svc.StockBarangSvc;

@Service(value="stockBarangSvc")
@Transactional
public class StockBarangSvcImpl implements StockBarangSvc {

	@Autowired
	private MstStockRepo mstStockRepo;
	
	public void save(MstStock stock) {
		mstStockRepo.save(stock);
	}

	public List<MstStock> saves(Collection<MstStock> stocks) {
		return mstStockRepo.save(stocks);
	}

	public List<MstStock> findAll() {
		return mstStockRepo.findAll();
	}
	
	public void delete(MstStock stock){
		mstStockRepo.delete(stock);
	}

}
