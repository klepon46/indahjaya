package com.indahjaya.svc.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.TrxBarangKeluarDtl;
import com.indahjaya.repo.TrxBarangKeluarDtlRepo;
import com.indahjaya.svc.BarangKeluarDtlSvc;

@Service(value="barangKeluarDtlSvc")
@Transactional
public class BarangKeluarDtlSvcImpl implements BarangKeluarDtlSvc{

	@Autowired
	private TrxBarangKeluarDtlRepo trxBarangKeluarDtlRepo;

	public void save(TrxBarangKeluarDtl object) {
		trxBarangKeluarDtlRepo.save(object);
	}

	public void saves(Collection<TrxBarangKeluarDtl> objects) {
		trxBarangKeluarDtlRepo.save(objects);
	}

	public List<TrxBarangKeluarDtl> findAll() {
		return trxBarangKeluarDtlRepo.findAll();
	}

}
