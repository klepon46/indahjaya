/**
 * 
 */
package com.indahjaya.svc.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.TrxBarangMasukDtl;
import com.indahjaya.repo.TrxBarangMasukDtlRepo;
import com.indahjaya.svc.BarangMasukDtlSvc;

/**
 * @author garya
 *
 */
@Service(value="barangMasukDtlSvc")
@Transactional
public class BarangMasukDtlSvcImpl implements BarangMasukDtlSvc{

	@Autowired
	private TrxBarangMasukDtlRepo trxBarangMasukDtlRepo;

	public void save(TrxBarangMasukDtl object) {
		trxBarangMasukDtlRepo.save(object);
	}

	public void saves(Collection<TrxBarangMasukDtl> objects) {
		trxBarangMasukDtlRepo.save(objects);
	}

	public List<TrxBarangMasukDtl> findAll() {
		return trxBarangMasukDtlRepo.findAll();
	}

}
