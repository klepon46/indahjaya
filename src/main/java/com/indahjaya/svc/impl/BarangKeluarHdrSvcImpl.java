/**
 * 
 */
package com.indahjaya.svc.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.TrxBarangKeluarHdr;
import com.indahjaya.repo.TrxBarangKeluarHdrRepo;
import com.indahjaya.svc.BarangKeluarHdrSvc;

/**
 * @author garya
 *
 */
@Service(value="barangKeluarHdrSvc")
@Transactional
public class BarangKeluarHdrSvcImpl implements BarangKeluarHdrSvc {

	@Autowired
	private TrxBarangKeluarHdrRepo trxBarangKeluarHdrRepo;
	
	public void save(TrxBarangKeluarHdr object) {
		trxBarangKeluarHdrRepo.save(object);
	}

	public void saves(Collection<TrxBarangKeluarHdr> objects) {
		trxBarangKeluarHdrRepo.save(objects);
	}

	public List<TrxBarangKeluarHdr> findAll() {
		return trxBarangKeluarHdrRepo.findAll();
	}

	

}
