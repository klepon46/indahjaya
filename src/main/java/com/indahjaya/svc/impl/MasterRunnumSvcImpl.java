package com.indahjaya.svc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.MstRunnum;
import com.indahjaya.repo.MstRunnumRepo;
import com.indahjaya.svc.MasterRunnumSvc;

@Service(value="masterRunnumSvc")
@Transactional
public class MasterRunnumSvcImpl implements MasterRunnumSvc{
	
	@Autowired
	private MstRunnumRepo mstRunnumRepo;
	
	public MstRunnum findByPrefix(String prefix) {
		return mstRunnumRepo.findByPrefix(prefix);
	}

	public void resetRunnum(String prefix, String midfix) {
		mstRunnumRepo.resetRunnum(prefix, midfix);
	}

	public void save(MstRunnum runnum) {
		mstRunnumRepo.save(runnum);
	}
	
	
}
