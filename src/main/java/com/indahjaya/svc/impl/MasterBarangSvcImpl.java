package com.indahjaya.svc.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.indahjaya.bean.MstBarang;
import com.indahjaya.bean.MstStock;
import com.indahjaya.repo.MstBarangRepo;
import com.indahjaya.repo.MstStockRepo;
import com.indahjaya.svc.MasterBarangSvc;

@Service(value="masterBarangSvc")
@Transactional
public class MasterBarangSvcImpl implements MasterBarangSvc {

	@Autowired
	private MstBarangRepo mstBarangRepo;
	
	@Autowired
	private MstStockRepo mstStockRepo;
	
	public void save(MstBarang mstBarang) {
		mstBarangRepo.save(mstBarang);
	}

	public List<MstBarang> findAll() {
		return mstBarangRepo.findAll();
	}

	public void delete(MstBarang mstBarang) {
		MstStock stock = new MstStock();
		stock.setKodeBarang(mstBarang.getKodeBarang());
		
		mstBarangRepo.delete(mstBarang);
		mstStockRepo.delete(stock);
	}

	public void update(MstBarang mstBarang) {
		
		MstStock stock =  mstStockRepo.findOne(mstBarang.getKodeBarang());
		if(stock != null){
			MstStock stockTosave = new MstStock();
			stockTosave.setKodeBarang(mstBarang.getKodeBarang());
			stockTosave.setNamaBarang(mstBarang.getNamaBarang());
			stockTosave.setQty(stock.getQty());
			stockTosave.setCreatedDate(stock.getCreatedDate());
			stockTosave.setUpdateDate(new Date());
			
			mstStockRepo.save(stockTosave);
			
		}
		
		mstBarangRepo.save(mstBarang);
	}
	
}
