package com.indahjaya.svc;

import java.util.Collection;
import java.util.List;

import com.indahjaya.bean.TrxBarangKeluarDtl;

public interface BarangKeluarDtlSvc {

	public void save(TrxBarangKeluarDtl object);
	public void saves(Collection<TrxBarangKeluarDtl> objects);
	public List<TrxBarangKeluarDtl> findAll();
	
}
