package com.indahjaya.svc;

import java.util.Collection;
import java.util.List;

import com.indahjaya.bean.TrxBarangMasukDtl;

public interface BarangMasukDtlSvc {

	public void save(TrxBarangMasukDtl object);
	public void saves(Collection<TrxBarangMasukDtl> objects);
	public List<TrxBarangMasukDtl> findAll();
	
}
