package com.indahjaya.svc;

import com.indahjaya.bean.MstRunnum;

public interface MasterRunnumSvc {

	public MstRunnum findByPrefix(String prefix);
	public void resetRunnum(String prefix, String midfix);
	public void save(MstRunnum runnum);
	
}
