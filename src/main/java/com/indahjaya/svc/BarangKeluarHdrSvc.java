/**
 * 
 */
package com.indahjaya.svc;

import java.util.Collection;
import java.util.List;

import com.indahjaya.bean.TrxBarangKeluarHdr;

/**
 * @author garya
 *
 */
public interface BarangKeluarHdrSvc {

	public void save(TrxBarangKeluarHdr object);
	public void saves(Collection<TrxBarangKeluarHdr> objects);
	public List<TrxBarangKeluarHdr> findAll();
	
}
