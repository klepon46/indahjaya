package com.indahjaya.svc;

import java.util.Collection;
import java.util.List;

import com.indahjaya.bean.MstStock;

public interface StockBarangSvc {
	
	public List<MstStock> findAll();
	public void save(MstStock stock);
	public List<MstStock> saves(Collection<MstStock> stocks);
	public void delete(MstStock stock);
	
}
