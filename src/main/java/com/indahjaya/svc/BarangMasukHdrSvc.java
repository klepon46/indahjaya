package com.indahjaya.svc;

import java.util.Collection;
import java.util.List;

import com.indahjaya.bean.TrxBarangMasukHdr;

public interface BarangMasukHdrSvc {

	public void save(TrxBarangMasukHdr object);
	public void saves(Collection<TrxBarangMasukHdr> objects);
	public List<TrxBarangMasukHdr> findAll();
	
}
