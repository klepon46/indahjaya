package com.indahjaya.svc;

import java.util.List;

import com.indahjaya.bean.MstBarang;

public interface MasterBarangSvc {

	public void save(MstBarang mstBarang);
	public List<MstBarang> findAll();
	public void delete(MstBarang mstBarang);
	public void update(MstBarang mstBarang);
	
}
