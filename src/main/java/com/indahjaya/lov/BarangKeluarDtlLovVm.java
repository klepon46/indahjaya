/**
 * 
 */
package com.indahjaya.lov;

import java.util.Set;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;

import com.indahjaya.bean.TrxBarangKeluarDtl;
import com.indahjaya.bean.TrxBarangKeluarHdr;

/**
 * @author garya
 *
 */
public class BarangKeluarDtlLovVm {
	
	private TrxBarangKeluarHdr barangKeluarHdr;
	private Set<TrxBarangKeluarDtl> barangKeluarDtls;
	
	@Init
	public void init(@ExecutionArgParam("hdr") TrxBarangKeluarHdr hdr){
		System.out.println("Kuda " + hdr);
		
		barangKeluarHdr = hdr;
		barangKeluarDtls = hdr.getTrxBarangKeluarDtls();
	}

	public TrxBarangKeluarHdr getBarangKeluarHdr() {
		return barangKeluarHdr;
	}

	public void setBarangKeluarHdr(TrxBarangKeluarHdr barangKeluarHdr) {
		this.barangKeluarHdr = barangKeluarHdr;
	}

	public Set<TrxBarangKeluarDtl> getBarangKeluarDtls() {
		return barangKeluarDtls;
	}

	public void setBarangKeluarDtls(Set<TrxBarangKeluarDtl> barangKeluarDtls) {
		this.barangKeluarDtls = barangKeluarDtls;
	}
	
}
