/**
 * 
 */
package com.indahjaya.lov;

import java.util.Set;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;

import com.indahjaya.bean.TrxBarangMasukDtl;
import com.indahjaya.bean.TrxBarangMasukHdr;

/**
 * @author garya
 *
 */
public class BarangMasukDtlLovVm {
	
	private TrxBarangMasukHdr barangMasukHdr;
	private Set<TrxBarangMasukDtl> barangMasukDtls;
	
	@Init
	public void init(@ExecutionArgParam("hdr") TrxBarangMasukHdr hdr){
		barangMasukHdr = hdr;
		barangMasukDtls = hdr.getTrxBarangMasukDtls();
	}

	public TrxBarangMasukHdr getBarangMasukHdr() {
		return barangMasukHdr;
	}

	public void setBarangMasukHdr(TrxBarangMasukHdr barangMasukHdr) {
		this.barangMasukHdr = barangMasukHdr;
	}

	public Set<TrxBarangMasukDtl> getBarangMasukDtls() {
		return barangMasukDtls;
	}

	public void setBarangMasukDtls(Set<TrxBarangMasukDtl> barangMasukDtls) {
		this.barangMasukDtls = barangMasukDtls;
	}
	
	
}
