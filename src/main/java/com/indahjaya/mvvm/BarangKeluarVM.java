package com.indahjaya.mvvm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

import com.indahjaya.bean.MstRunnum;
import com.indahjaya.bean.MstStock;
import com.indahjaya.bean.TrxBarangKeluarDtl;
import com.indahjaya.bean.TrxBarangKeluarHdr;
import com.indahjaya.svc.BarangKeluarDtlSvc;
import com.indahjaya.svc.BarangKeluarHdrSvc;
import com.indahjaya.svc.MasterRunnumSvc;
import com.indahjaya.svc.StockBarangSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class BarangKeluarVM {

	@WireVariable
	private StockBarangSvc stockBarangSvc;

	@WireVariable
	private BarangKeluarDtlSvc barangKeluarDtlSvc;

	@WireVariable
	private BarangKeluarHdrSvc barangKeluarHdrSvc;

	@WireVariable
	private MasterRunnumSvc masterRunnumSvc;

	private List<MstStock> stocks;
	private Set<TrxBarangKeluarDtl> barangKeluars;
	private List<TrxBarangKeluarHdr> barangKeluarsHdr;

	private MstStock selectedStock;
	private TrxBarangKeluarDtl brgKeluarDtl;
	private TrxBarangKeluarHdr brgKeluarHdr;
	private MstRunnum runningNumber;

	@Init
	public void init(){
		brgKeluarDtl = new TrxBarangKeluarDtl();
		brgKeluarHdr = new TrxBarangKeluarHdr();

		stocks = stockBarangSvc.findAll();
		barangKeluarsHdr = barangKeluarHdrSvc.findAll();
		Collections.reverse(barangKeluarsHdr);

		barangKeluars = new HashSet<TrxBarangKeluarDtl>();
		runningNumber = initRunnum();
	}


	@Command
	@NotifyChange("barangKeluars")
	public void addItem(@BindingParam("me")MstStock stock){

		TrxBarangKeluarDtl out = new TrxBarangKeluarDtl();
		out.getPk().setKodeBarang(stock.getKodeBarang());
		out.getPk().setKodeTransaksi(brgKeluarHdr.getKodeTransaksi());
		out.setNamaBarang(stock.getNamaBarang());
		out.setCreatedDate(new Date());
		out.setQty(0);

		if(barangKeluars.contains(out)){

			Clients.showNotification("Item Sudah Ada", "warning", null, null, 1500);
			return;
		}

		barangKeluars.add(out);


	}

	@Command
	@NotifyChange("barangKeluars")
	public void hapusDariListToSave(@BindingParam("me") TrxBarangKeluarDtl dtl){
		barangKeluars.remove(dtl);
	}

	@Command
	public void changeQty(@BindingParam("me") TrxBarangKeluarDtl keluarDtl){

	}

	@Command
	@NotifyChange({"stocks","barangKeluars","barangKeluarsHdr"})
	public void save(@ContextParam(ContextType.BINDER) Binder binder){

		int jumlahQty = 0;
		for(TrxBarangKeluarDtl dtl : barangKeluars){
			jumlahQty = jumlahQty + dtl.getQty();

			MstStock tempStock = new MstStock();
			tempStock.setKodeBarang(dtl.getPk().getKodeBarang());
			MstStock stck = stocks.get(stocks.indexOf(tempStock));

			stck.setQty(stck.getQty() - dtl.getQty());
		}

		brgKeluarHdr.setJumlahBarang(jumlahQty);
		brgKeluarHdr.setTrxBarangKeluarDtls(barangKeluars);

		stockBarangSvc.saves(stocks);
		barangKeluarHdrSvc.save(brgKeluarHdr);
		masterRunnumSvc.save(runningNumber);

		binder.sendCommand("initRunnum", null);

		barangKeluarsHdr = barangKeluarHdrSvc.findAll();
		Collections.reverse(barangKeluarsHdr);

		barangKeluars = new HashSet<TrxBarangKeluarDtl>();
	}

	/*
	 * dieksekusi ketika user menekan tombol detail di history listbox
	 */
	@Command
	public void showDetail(@BindingParam("me")TrxBarangKeluarHdr hdr){

		Map<String, TrxBarangKeluarHdr> map = new HashMap<String, TrxBarangKeluarHdr>();
		map.put("hdr", hdr);

		Executions.createComponents("/lov/barangKeluarLov.zul", null, map);
	}

	//method yang dieksekusi ketika user melakukan searching di kolom search
	@Command
	@NotifyChange("stocks")
	@SuppressWarnings("unchecked")
	public void searchAll(@BindingParam("search") final String search){
		Collection<MstStock> l =	CollectionUtils.select(stocks, new Predicate() {
			public boolean evaluate(Object arg0) {
				MstStock asx = (MstStock)arg0;
				return asx.getNamaBarang().toUpperCase().matches("(.*)"+search.toUpperCase()+"(.*)");
			}
		});

		stocks = new ArrayList<MstStock>(l);
		
	}

	//dieksekusi pada saat mengisi kolom search
	@Command
	public void onChanging(){
		stocks = stockBarangSvc.findAll();
	}

	@Command
	@NotifyChange("brgKeluarHdr")
	public MstRunnum initRunnum(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String todayDate = sdf.format(new Date());
		MstRunnum runnum = masterRunnumSvc.findByPrefix("BK");
		String trxId = "";

		if(todayDate.equalsIgnoreCase(runnum.getMidfix())){
			trxId = runnum.getPrefix()+"-"+runnum.getMidfix()
					+"-"+olahNumber(runnum.getSequence() + 1);

			runnum.setSequence(runnum.getSequence()+1);
		}else{

			//reset seq ke 0 jika tanggal tidak sama
			masterRunnumSvc.resetRunnum("BK", todayDate);

			trxId = runnum.getPrefix()+"-"+todayDate
					+"-"+olahNumber(1);

			runnum.setMidfix(todayDate);
			runnum.setSequence(1);
		}

		brgKeluarHdr.setKodeTransaksi(trxId);
		return runnum;
	}

	private String olahNumber(Integer runNumber){

		int length = String.valueOf(runNumber).length();
		StringBuilder sb = new StringBuilder();

		if(length == 1){
			sb.append("00");
			sb.append(runNumber);
		}else if(length==2){
			sb.append("0");
			sb.append(runNumber);
		}else{
			sb.append(runNumber);
		}

		return sb.toString();
	}

	public List<MstStock> getStocks() {
		return stocks;
	}

	public void setStocks(List<MstStock> stocks) {
		this.stocks = stocks;
	}

	public MstStock getSelectedStock() {
		return selectedStock;
	}

	public void setSelectedStock(MstStock selectedStock) {
		this.selectedStock = selectedStock;
	}

	public Set<TrxBarangKeluarDtl> getBarangKeluars() {
		return barangKeluars;
	}
	public void setBarangKeluars(Set<TrxBarangKeluarDtl> barangKeluars) {
		this.barangKeluars = barangKeluars;
	}
	public TrxBarangKeluarDtl getBrgKeluarDtl() {
		return brgKeluarDtl;
	}

	public void setBrgKeluarDtl(TrxBarangKeluarDtl brgKeluarDtl) {
		this.brgKeluarDtl = brgKeluarDtl;
	}

	public TrxBarangKeluarHdr getBrgKeluarHdr() {
		return brgKeluarHdr;
	}

	public void setBrgKeluarHdr(TrxBarangKeluarHdr brgKeluarHdr) {
		this.brgKeluarHdr = brgKeluarHdr;
	}


	public List<TrxBarangKeluarHdr> getBarangKeluarsHdr() {
		return barangKeluarsHdr;
	}


	public void setBarangKeluarsHdr(List<TrxBarangKeluarHdr> barangKeluarsHdr) {
		this.barangKeluarsHdr = barangKeluarsHdr;
	}


}
