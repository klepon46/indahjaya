/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package com.indahjaya.mvvm;

import java.util.List;

import com.indahjaya.util.SidebarPage;
import com.indahjaya.util.SidebarPageConfig;

public class SidebarAjaxbasedViewModel {

	//todo: wire service
	//populate the menu in the constructor
	private SidebarPageConfig pageConfig = new SidebarPageConfigAjaxBasedImpl();
	
	public List<SidebarPage> getSidebarPages() {
		return pageConfig.getPages();
	}
}
