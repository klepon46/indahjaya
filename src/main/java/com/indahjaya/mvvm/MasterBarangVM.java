package com.indahjaya.mvvm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

import com.indahjaya.bean.MstBarang;
import com.indahjaya.svc.MasterBarangSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class MasterBarangVM {

	@WireVariable
	private MasterBarangSvc masterBarangSvc;

	private MstBarang mstBarang;
	private List<MstBarang> barangs;

	@Init
	public void init(){
		barangs = new ArrayList<MstBarang>();
		barangs = masterBarangSvc.findAll();
	}

	@Command
	@NotifyChange("barangs")
	public void save(){
		mstBarang.setCreatedDate(new Date());
		masterBarangSvc.save(mstBarang);

		barangs = new ArrayList<MstBarang>();
		barangs = masterBarangSvc.findAll();
	}
	
	@Command
	@NotifyChange("barangs")
	public void hapusBarang(@BindingParam("me") MstBarang mstBarang){
		masterBarangSvc.delete(mstBarang);
		barangs.remove(mstBarang);
	}
	
	@Command
	@NotifyChange("barangs")
	public void updateBarang(@BindingParam("me") MstBarang mstBarang){
		masterBarangSvc.update(mstBarang);
	}

	public MstBarang getMstBarang() {
		if(mstBarang == null)
			mstBarang = new MstBarang();
		return mstBarang;
	}

	public void setMstBarang(MstBarang mstBarang) {
		this.mstBarang = mstBarang;
	}

	public List<MstBarang> getBarangs() {
		return barangs;
	}

	public void setBarangs(List<MstBarang> barangs) {
		this.barangs = barangs;
	}

}
