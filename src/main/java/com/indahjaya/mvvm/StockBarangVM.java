package com.indahjaya.mvvm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Filedownload;

import com.indahjaya.bean.MstBarang;
import com.indahjaya.bean.MstStock;
import com.indahjaya.svc.MasterBarangSvc;
import com.indahjaya.svc.StockBarangSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class StockBarangVM {

	@WireVariable
	private MasterBarangSvc masterBarangSvc;

	@WireVariable
	private StockBarangSvc stockBarangSvc;

	private List<MstStock> stockTerisi;

	@Init
	public void init(){
		stockTerisi = new ArrayList<MstStock>();
		stockTerisi = stockBarangSvc.findAll();
	}

	@Command
	public void dlTemplate() throws IOException{
		List<MstBarang> barangs = new ArrayList<MstBarang>();
		barangs = masterBarangSvc.findAll();

		FileOutputStream fileOutput = new FileOutputStream("template.xls");
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Sheet 1");

		int rowIndex = 0;

		HSSFRow row = sheet.createRow(rowIndex);
		HSSFCell cell = row.createCell(0);

		for(MstBarang brg : barangs){
			row = sheet.createRow(rowIndex++);
			cell = row.createCell(0);
			cell.setCellValue(brg.getKodeBarang());

			cell = row.createCell(1);
			cell.setCellValue(brg.getNamaBarang());

			cell = row.createCell(2);

		}

		wb.write(fileOutput);
		fileOutput.close();

		File file = new File("template.xls");
		Filedownload.save(file,"XLS");
	}
	
	@Command
	public void dlCurrentStock() throws IOException{

		FileOutputStream fileOutput = new FileOutputStream("stock.xls");
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Sheet 1");

		int rowIndex = 0;

		HSSFRow row = sheet.createRow(rowIndex);
		HSSFCell cell = row.createCell(0);

		for(MstStock brg : stockTerisi){
			row = sheet.createRow(rowIndex++);
			cell = row.createCell(0);
			cell.setCellValue(brg.getKodeBarang());

			cell = row.createCell(1);
			cell.setCellValue(brg.getNamaBarang());

			cell = row.createCell(2);
			cell.setCellValue(brg.getQty());

		}

		wb.write(fileOutput);
		fileOutput.close();

		File file = new File("stock.xls");
		Filedownload.save(file,"XLS");
	}

	@Command
	@NotifyChange("stockTerisi")
	@SuppressWarnings("unchecked")
	public void searchAll(@BindingParam("search") final String search){
		Collection<MstStock> l =	CollectionUtils.select(stockTerisi, new Predicate() {
			public boolean evaluate(Object arg0) {
				MstStock asx = (MstStock)arg0;
				return asx.getNamaBarang().toUpperCase().matches("(.*)"+search.toUpperCase()+"(.*)");
			}
		});

		stockTerisi = new ArrayList<MstStock>(l);

	}

	//dieksekusi pada saat mengisi kolom search
	@Command
	public void onChanging(){
		stockTerisi = stockBarangSvc.findAll();
	}

	@Command
	@NotifyChange("stockTerisi")
	public void upTemplate(@BindingParam("media")Media media) throws IOException{
		List<MstStock> stocks = new ArrayList<MstStock>();

		String webAppPath =  Executions.getCurrent().getDesktop()
				.getWebApp().getRealPath("/")+"\\";
		String filePath = webAppPath+media.getName();
		Files.copy(new File(filePath), media.getStreamData());

		File file = new File(filePath);
		FileInputStream fis = new FileInputStream(file.getAbsolutePath());
		POIFSFileSystem fs = new POIFSFileSystem(fis);
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row;


		for (int i = 0; i <= sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);

			String kodeBarang = row.getCell(0).getStringCellValue();
			String namaBarang = row.getCell(1).getStringCellValue();
			Double qty =  row.getCell(2).getNumericCellValue();

			if( qty != 0 || qty != null){
				MstStock stock = new MstStock();
				stock.setKodeBarang(kodeBarang);
				stock.setNamaBarang(namaBarang);
				stock.setQty(qty.intValue());
				stocks.add(stock);
			}
		}

		stockBarangSvc.saves(stocks);

		stockTerisi = new ArrayList<MstStock>();
		stockTerisi = stockBarangSvc.findAll();

	}
	
	@Command
	@NotifyChange("stockTerisi")
	public void delete(@BindingParam("me")MstStock stock){
		stockBarangSvc.delete(stock);
		stockTerisi.remove(stock);
	}

	public List<MstStock> getStockTerisi() {
		return stockTerisi;
	}

	public void setStockTerisi(List<MstStock> stockTerisi) {
		this.stockTerisi = stockTerisi;
	}

}
