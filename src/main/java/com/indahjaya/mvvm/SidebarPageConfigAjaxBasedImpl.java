/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package com.indahjaya.mvvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.indahjaya.util.SidebarPage;
import com.indahjaya.util.SidebarPageConfig;

public class SidebarPageConfigAjaxBasedImpl implements SidebarPageConfig{
	
	HashMap<String,SidebarPage> pageMap = new LinkedHashMap<String,SidebarPage>();
	public SidebarPageConfigAjaxBasedImpl(){		
//		pageMap.put("zk",new SidebarPage("zk","www.zkoss.org","/imgs/site.png","http://www.zkoss.org/"));
//		pageMap.put("demo",new SidebarPage("demo","ZK Demo","/imgs/demo.png","http://www.zkoss.org/zkdemo"));
//		pageMap.put("devref",new SidebarPage("devref","ZK Developer Reference","/imgs/doc.png","http://books.zkoss.org/wiki/ZK_Developer's_Reference"));
		
		pageMap.put("fn1",new SidebarPage("fn1","Master Barang","/imgs/fn.png","/master/masterBarang.zul"));
		pageMap.put("fn2",new SidebarPage("fn2","Stock Barang","/imgs/fn.png","/master/stockBarang.zul"));
		pageMap.put("fn3",new SidebarPage("fn3","Barang Masuk","/imgs/fn.png","/trx/barangMasuk.zul"));
		pageMap.put("fn4",new SidebarPage("fn4","Barang Keluar","/imgs/fn.png","/trx/barangKeluar.zul"));
	}
	
	public List<SidebarPage> getPages(){
		return new ArrayList<SidebarPage>(pageMap.values());
	}
	
	public SidebarPage getPage(String name){
		return pageMap.get(name);
	}
	
}