package com.indahjaya.mvvm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

import com.indahjaya.bean.MstRunnum;
import com.indahjaya.bean.MstStock;
import com.indahjaya.bean.TrxBarangMasukDtl;
import com.indahjaya.bean.TrxBarangMasukHdr;
import com.indahjaya.svc.BarangMasukHdrSvc;
import com.indahjaya.svc.MasterRunnumSvc;
import com.indahjaya.svc.StockBarangSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class BarangMasukVM {

	@WireVariable
	private StockBarangSvc stockBarangSvc;

	@WireVariable
	private BarangMasukHdrSvc barangMasukHdrSvc;

	@WireVariable
	private MasterRunnumSvc masterRunnumSvc;

	private List<MstStock> stocks;
	private Set<TrxBarangMasukDtl> barangMasuks;
	private List<TrxBarangMasukHdr> barangMasuksHdr;

	private MstStock selectedStock;
	private TrxBarangMasukHdr brgMasukHdr;
	private MstRunnum runningNumber;

	@Init
	public void init(){
		brgMasukHdr = new TrxBarangMasukHdr();
		stocks = stockBarangSvc.findAll();
		barangMasuks = new HashSet<TrxBarangMasukDtl>();
		barangMasuksHdr = barangMasukHdrSvc.findAll();
		Collections.reverse(barangMasuksHdr);

		runningNumber = initRunnum();
	}

	//masuk ke dalam list to Save
	@Command
	@NotifyChange("barangMasuks")
	public void addItem(@BindingParam("me")MstStock stock){

		TrxBarangMasukDtl in = new TrxBarangMasukDtl();
		in.getPk().setKodeBarang(stock.getKodeBarang());
		in.getPk().setKodeTransaksi(brgMasukHdr.getKodeTransaksi());
		in.setNamaBarang(stock.getNamaBarang());
		in.setCreatedDate(new Date());
		in.setQty(0);

		if(barangMasuks.contains(in)){

			Clients.showNotification("Item Sudah Ada", "warning", null, null, 1500);
			return;
		}

		barangMasuks.add(in);
	}

	@Command
	@NotifyChange("barangMasuks")
	public void hapusDariListToSave(@BindingParam("me") TrxBarangMasukDtl dtl){
		barangMasuks.remove(dtl);
	}

	@Command
	@NotifyChange({"stocks","barangMasuksHdr","barangMasuks"})
	public void save(@ContextParam(ContextType.BINDER) Binder binder){

		int jumlahQty = 0;
		for(TrxBarangMasukDtl dtl : barangMasuks){
			jumlahQty = jumlahQty + dtl.getQty();

			MstStock tempStock = new MstStock();
			tempStock.setKodeBarang(dtl.getPk().getKodeBarang());
			MstStock stck = stocks.get(stocks.indexOf(tempStock));

			stck.setQty(stck.getQty() + dtl.getQty());
		}

		brgMasukHdr.setJumlahBarang(jumlahQty);
		brgMasukHdr.setTrxBarangMasukDtls(barangMasuks);

		stockBarangSvc.saves(stocks);
		barangMasukHdrSvc.save(brgMasukHdr);
		masterRunnumSvc.save(runningNumber);

		binder.sendCommand("initRunnum", null);

		barangMasuksHdr = barangMasukHdrSvc.findAll();
		Collections.reverse(barangMasuksHdr);

		barangMasuks = new HashSet<TrxBarangMasukDtl>();

	}

	@Command
	@NotifyChange("stocks")
	@SuppressWarnings("unchecked")
	public void searchAll(@BindingParam("search") final String search){
		Collection<MstStock> l =	CollectionUtils.select(stocks, new Predicate() {
			public boolean evaluate(Object arg0) {
				MstStock asx = (MstStock)arg0;
				return asx.getNamaBarang().toUpperCase().matches("(.*)"+search.toUpperCase()+"(.*)");
			}
		});

		stocks = new ArrayList<MstStock>(l);

	}

	//dieksekusi pada saat mengisi kolom search
	@Command
	public void onChanging(){
		stocks = stockBarangSvc.findAll();
	}

	/*
	 * dieksekusi ketika user menekan tombol detail di history listbox
	 */
	@Command
	public void showDetail(@BindingParam("me") TrxBarangMasukHdr hdr){

		Map<String, TrxBarangMasukHdr> map = new HashMap<String, TrxBarangMasukHdr>();
		map.put("hdr", hdr);

		Executions.createComponents("/lov/barangMasukLov.zul", null, map);

	}

	@Command
	@NotifyChange("brgMasukHdr")
	public MstRunnum initRunnum(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String todayDate = sdf.format(new Date());
		MstRunnum runnum = masterRunnumSvc.findByPrefix("BM");
		String trxId = "";

		if(todayDate.equalsIgnoreCase(runnum.getMidfix())){
			trxId = runnum.getPrefix()+"-"+runnum.getMidfix()
					+"-"+olahNumber(runnum.getSequence() + 1);

			runnum.setSequence(runnum.getSequence()+1);
		}else{

			//reset seq ke 0 jika tanggal tidak sama
			masterRunnumSvc.resetRunnum("BM", todayDate);

			trxId = runnum.getPrefix()+"-"+todayDate
					+"-"+olahNumber(1);

			runnum.setMidfix(todayDate);
			runnum.setSequence(1);
		}

		brgMasukHdr.setKodeTransaksi(trxId);
		return runnum;
	}

	private String olahNumber(Integer runNumber){

		int length = String.valueOf(runNumber).length();
		StringBuilder sb = new StringBuilder();

		if(length == 1){
			sb.append("00");
			sb.append(runNumber);
		}else if(length==2){
			sb.append("0");
			sb.append(runNumber);
		}else{
			sb.append(runNumber);
		}

		return sb.toString();
	}

	public List<MstStock> getStocks() {
		return stocks;
	}

	public void setStocks(List<MstStock> stocks) {
		this.stocks = stocks;
	}

	public MstStock getSelectedStock() {
		return selectedStock;
	}

	public void setSelectedStock(MstStock selectedStock) {
		this.selectedStock = selectedStock;
	}

	public TrxBarangMasukHdr getBrgMasukHdr() {
		return brgMasukHdr;
	}

	public void setBrgMasukHdr(TrxBarangMasukHdr brgMasukHdr) {
		this.brgMasukHdr = brgMasukHdr;
	}

	public List<TrxBarangMasukHdr> getBarangMasuksHdr() {
		return barangMasuksHdr;
	}

	public void setBarangMasuksHdr(List<TrxBarangMasukHdr> barangMasuksHdr) {
		this.barangMasuksHdr = barangMasuksHdr;
	}

	public Set<TrxBarangMasukDtl> getBarangMasuks() {
		return barangMasuks;
	}

	public void setBarangMasuks(Set<TrxBarangMasukDtl> barangMasuks) {
		this.barangMasuks = barangMasuks;
	}	

}
