package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indahjaya.bean.MstBarang;

@Repository
public interface MstBarangRepo extends JpaRepository<MstBarang, String>{

}
