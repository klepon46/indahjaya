/**
 * 
 */
package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indahjaya.bean.TrxBarangKeluarDtl;

/**
 * @author garya
 *
 */
public interface TrxBarangKeluarDtlRepo extends JpaRepository<TrxBarangKeluarDtl, String> {

}
