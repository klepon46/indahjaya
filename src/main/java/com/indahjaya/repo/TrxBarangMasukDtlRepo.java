/**
 * 
 */
package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indahjaya.bean.TrxBarangMasukDtl;

/**
 * @author garya
 *
 */
public interface TrxBarangMasukDtlRepo extends JpaRepository<TrxBarangMasukDtl, String>{

}
