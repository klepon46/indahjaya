package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.indahjaya.bean.MstRunnum;

@Repository
public interface MstRunnumRepo extends JpaRepository<MstRunnum, String> {

	@Query(value="select r from MstRunnum r where r.prefix = :prefix ")
	public MstRunnum findByPrefix(@Param("prefix") String prefix);
	
	@Modifying
	@Query(value="update MstRunnum set sequence = 0 ,"
			+ "midfix = :midfix "
			+ "where prefix = :prefix ")
	public void resetRunnum(
			@Param("prefix") String prefix,
			@Param("midfix")String midfix);
	
	

}
