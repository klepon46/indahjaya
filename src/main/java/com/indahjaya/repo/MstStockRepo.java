package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indahjaya.bean.MstStock;

@Repository
public interface MstStockRepo extends JpaRepository<MstStock, String> {

}
