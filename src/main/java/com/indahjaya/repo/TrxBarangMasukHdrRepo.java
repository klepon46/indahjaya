package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indahjaya.bean.TrxBarangMasukHdr;

public interface TrxBarangMasukHdrRepo extends JpaRepository<TrxBarangMasukHdr, String> {

}
