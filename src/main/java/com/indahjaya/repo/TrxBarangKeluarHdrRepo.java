package com.indahjaya.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indahjaya.bean.TrxBarangKeluarHdr;

public interface TrxBarangKeluarHdrRepo extends JpaRepository<TrxBarangKeluarHdr, String> {

}
